
(function(){
var scroll = new SmoothScroll('a[href*="#"]', {
    // Selectors
    ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
    header: null, // Selector for fixed headers (must be a valid CSS selector)

    // Speed & Easing
    speed: 800, // Integer. How fast to complete the scroll in milliseconds
    offset: 25, // Integer or Function returning an integer. How far to offset the scrolling anchor location in pixels
    easing: 'easeInOutCubic', // Easing pattern to use
    customEasing: function (time) {}, // Function. Custom easing pattern

    // Callback API
    before: function () {}, // Callback to run before scroll
    after: function () {} // Callback to run after scroll
});



var rellax = new Rellax('.rellax');


var body = document.querySelector('body');
var navigation = document.querySelector('.navigation');
var button = document.querySelector('#nav-button');
var menu = document.querySelector('.navigation__menu');
var loader = document.querySelector('#loader');
var state= false;

var mobile_nav = function () {
    if (state === false) {
        menu.classList.add('show')
        state = true;
    } else {
        menu.classList.remove('show')
        state = false;
    }
}
//  hide menu on click link.
var links = document.querySelectorAll('.navigation__a');
button.addEventListener('click', mobile_nav);
links.forEach(function (el) {
    el.addEventListener('click', function () {
        menu.classList.remove('show');
        state = false;
    });
});

//  hide menu on click anywhere.

var sections = document.querySelectorAll('section');
sections.forEach(function (el) {
    el.addEventListener('click', function () {
        menu.classList.remove('show');
        state = false;
    });
});

// rellax -- parallax

var rellax = new Rellax('.rellax');
var size = window.innerHeight -200;

document.addEventListener('scroll', function() {
    let posY = window.scrollY;
    if(posY < size) {
        if(navigation.classList.contains('nav-pinned')) {
            navigation.classList.remove('nav-pinned')
        }
        navigation.classList.add('nav-unpinned');
        console.log(posY)
    }
    else if ( posY > size ) {
        if(navigation.classList.contains('nav-unpinned')) {
            navigation.classList.remove('nav-unpinned')
        }
        navigation.classList.add('nav-pinned');
    }
})




window.addEventListener('DOMContentLoaded',function(){
    new SmartPhoto(".js-smartPhoto");
});

// scroll reveal

window.sr = ScrollReveal();
sr.reveal('.section', { duration:700, distance: '60px', opacity: 1,scale: 1 , delay: 350});
sr.reveal('.gallery-link', { duration:700, distance: '60px', opacity: 1,scale: 1 , delay: 350})

var loaderDone = function() {   
    loader.classList.add('loader-done')    
}

window.addEventListener('load', loaderDone);
button.addEventListener('click', mobile_nav);


}());