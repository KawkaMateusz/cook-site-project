var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
let cleanCSS = require('gulp-clean-css');
var babel = require('gulp-babel');
gulp.task('prefix', () =>
    gulp.src('src/stylesheets/style.css')
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
);

gulp.task('minify-css', () => {
  return gulp.src('dist/style.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist'));
});


gulp.task('babel', () =>
    gulp.src('src/scripts/main.js')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('dist'))
);